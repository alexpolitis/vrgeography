﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {
	[SerializeField] private GameObject instantiatePos;
	[SerializeField] private GameObject bulletPrefab;
	public int count = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Cardboard.SDK.Triggered) {
			count++;
			shootBullet();
		}
	}

	public void shootBullet() {
		GameObject bullet =	Instantiate(bulletPrefab, instantiatePos.transform.position, Quaternion.identity) as GameObject;
		bullet.GetComponent<Rigidbody>().AddForce(transform.forward * 20f, ForceMode.Impulse);
	}

}
