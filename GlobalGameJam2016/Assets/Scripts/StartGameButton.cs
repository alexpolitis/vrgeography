﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartGameButton : MonoBehaviour {

	public GameObject QuizManagerPrefab;
	[SerializeField] private AudioSource audSource;
	[SerializeField] private AudioClip rolloverSound;

	public void StartGame() {
		GameObject quizManager = Instantiate(QuizManagerPrefab, transform.position, Quaternion.identity) as GameObject;
		quizManager.GetComponent<QuizManager>().loadNextLevel();
	}

	public void playRolloverSound() {
		audSource.PlayOneShot(rolloverSound);
	}
}
