﻿using UnityEngine;
using System.Collections;

public class VRGeoButton : MonoBehaviour {

	[SerializeField] private SpriteRenderer _spriteRenderer;

	private CardboardHead head;
	private Vector3 startingPosition;
	private float delay = 0.0f; 

	[SerializeField] private Color _normalColor;
	[SerializeField] private Color _highlightedColor;
	[SerializeField] private Color _clickedColor;
	public bool triggeredButton = false;


	public virtual void Start() {
		head = Camera.main.GetComponent<StereoController>().Head;
		startingPosition = transform.localPosition;
	}

	void Update() {
		RaycastHit hit;
		bool isLookedAt = GetComponent<Collider>().Raycast(head.Gaze, out hit, Mathf.Infinity);
		// if looking at object for 2 seconds, enable/disable autowalk

		if (isLookedAt) { 

			if (!triggeredButton) {
				doHighlight();
			}
				
			if (Cardboard.SDK.Triggered) {
				triggeredButton = true; 
				doClick();
			}

		} else if (!isLookedAt) { 

			doUnhighlight();
		
		}
	}

	public virtual void doHighlight() {
		_spriteRenderer.color = Color.yellow; 

	}

	public virtual void doClick() {
		_spriteRenderer.color =  Color.green; 

	}

	public virtual void doUnhighlight() {
		triggeredButton = false;
		_spriteRenderer.color = Color.red; 
	}

}
