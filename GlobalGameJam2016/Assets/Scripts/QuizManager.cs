﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class QuizManager : MonoBehaviour {

	[SerializeField] private AudioSource audSource;
	[SerializeField] private AudioClip bellClip;
	QuizSceneManager currentSceneManager;
	public List<string> landmarks  = new List<string> {"Machu Picchu", "Mecca", "Pyramids of Giza", "Statue of Liberty", "Plaza de Mayo"};
	public List<string> landmarksToPass = new List<string>();
	public List<string> scenesList;
	int numScenesToShow;
    bool vrModeEnabled;


    public float score;
	public float maxScore;

	void Awake() {
		landmarks  = new List<string> {"Machu Picchu", "Mecca", "Pyramids of Giza", "Statue of Liberty", "Plaza de Mayo"};
		score = 0;
		DontDestroyOnLoad(transform.gameObject);
		numScenesToShow = 5;
		maxScore = numScenesToShow * 3;
		getSceneList(numScenesToShow);
        vrModeEnabled = Cardboard.SDK.VRModeEnabled;
    }

	void getSceneList(int numScenes) {
		
		for (int i = 0; i < numScenes; i++) {
			scenesList.Add(landmarks[i]);
		} 
	}

	public void loadNextLevel() {

		if (scenesList.Count > 0) {
			string sceneToLoad = getRandomScene();
			SceneManager.LoadScene(sceneToLoad);
        } else {
			SceneManager.LoadScene("GameOver");
		}

	}

	public void CheckAnswer(string answer) {
		if (answer == currentSceneManager.landmarkName) {
			score += currentSceneManager.numTries - 1;
			currentSceneManager.setInformationalText(currentSceneManager.longText);
			currentSceneManager.showExplosion();
			currentSceneManager.showNextScenePanel();
			currentSceneManager.hideQuizButtonPanel();
			audSource.PlayOneShot(bellClip);
		} else {
			currentSceneManager.numTries -= 1;
		}
	}

	void OnLevelWasLoaded(int level) {

        if (!vrModeEnabled)
        {
            Cardboard.SDK.VRModeEnabled = false;
        }

        if (SceneManager.GetActiveScene().name != "GameOver") {
			currentSceneManager = GameObject.Find("QuizSceneManager").GetComponent<QuizSceneManager>();
			currentSceneManager.quizManager = this;
			copyLandmarkList();
			currentSceneManager.populateButtons(landmarksToPass);	
			currentSceneManager.setInformationalText(currentSceneManager.shortText);
		} 

		if (SceneManager.GetActiveScene().name == "GameOver") {
			setFinalScoreText();
		}


	}

	string getRandomScene() {
		string randomScene = scenesList[Random.Range (0, scenesList.Count)];
		scenesList.Remove(randomScene);
		return randomScene;
	}

	void copyLandmarkList() {
		landmarksToPass.Clear();
		landmarksToPass.AddRange(landmarks);

	}
		
	List<int> getUniqueRandomIndeces (int numIndecesToGet, int listCount){
		List<int> potentialIndeces = new List<int> ();
		List<int> finalUniqueIndeces = new List<int> ();

		for (int i = 0; i < listCount; i++) {
			potentialIndeces.Add (i);
		}

		for (int i = 0; i < numIndecesToGet; i++) {
			int randNum = potentialIndeces[Random.Range(0,potentialIndeces.Count)];
			finalUniqueIndeces.Add (randNum);
			potentialIndeces.Remove (randNum);
		}

		return finalUniqueIndeces;

	}

	void setFinalScoreText() {
		TextMeshProUGUI finalScoreText = GameObject.Find ("FinalScoreText").GetComponent<TextMeshProUGUI>();
		TextMeshProUGUI finalRankText = GameObject.Find ("FinalRankText").GetComponent<TextMeshProUGUI>();

		int finalScore = Mathf.RoundToInt((score / maxScore) * 100);

		finalScoreText.text = finalScore.ToString() + "%";

		if (finalScore > 90) 
		{
			finalRankText.text = "Global Guru";
		} 
		else if ( finalScore > 80 && finalScore <= 90 ) 
		{
			finalRankText.text = "Frequent Flyer";

		} else if ( finalScore > 70 && finalScore <= 80 ) 
		{
			finalRankText.text = "Timid Tourist";
				
		} else if ( finalScore <= 70  ) {
			finalRankText.text = "Local Joe";

		} 


	}




}
