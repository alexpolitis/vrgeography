﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class QuizButton : MonoBehaviour {

	[SerializeField] private Button _button;
	public string answer;
	[SerializeField] private QuizManager _quizManager;
	[SerializeField] private TextMeshProUGUI _answerText;

	public bool isCorrect = false;

	void Start () {
		_quizManager = GameObject.FindGameObjectWithTag ("QuizManager").GetComponent<QuizManager>();
	}

	public void SendAnswer() {
		_quizManager.CheckAnswer(answer);

		if (!isCorrect) {
			hideButton();
		}

	}

	public void SetAnswer(string ans) {
		answer = ans;
		_answerText.text = ans;

	}

	public void hideButton() {
		_button.interactable = false;
	}
}
