﻿using UnityEngine;
using System.Collections;

public class ToggleVRButton : MonoBehaviour {

	public void ToggleVRMode() {
		Cardboard.SDK.VRModeEnabled = !Cardboard.SDK.VRModeEnabled;
	}
		
}