﻿using UnityEngine;
using System.Collections;

public class CardboardObjInteracter2D : MonoBehaviour {
	private CardboardHead head;
	private Vector3 startingPosition;
	private float delay = 0.0f; 

	public bool triggeredButton = false;


	void Start() {
		head = Camera.main.GetComponent<StereoController>().Head;
		startingPosition = transform.localPosition;
	}

	void Update() {
		RaycastHit hit;
		bool isLookedAt = GetComponent<Collider>().Raycast(head.Gaze, out hit, Mathf.Infinity);
		// if looking at object for 2 seconds, enable/disable autowalk
		if (isLookedAt && Time.time>delay) { 
			GameObject FPSController = GameObject.Find("Head");

			//			FPSInputController autowalk = FPSController.GetComponent<FPSInputController>();
			//			autowalk.checkAutoWalk = !autowalk.checkAutoWalk;
			delay = Time.time + 2.0f;
		}
		// currently looking at object
		else if (isLookedAt) { 
			if (!triggeredButton) {
				GetComponent<SpriteRenderer>().color = Color.yellow;
				//GetComponent<Spritr>().material.color = Color.yellow; 
			}


			if (Cardboard.SDK.Triggered) {
				triggeredButton = true; 

				GetComponent<SpriteRenderer>().color = Color.green;

//				GetComponent<Renderer>().material.color = Color.green; 
			}

		} 
		// not looking at object
		else if (!isLookedAt) { 
			GetComponent<SpriteRenderer>().color = Color.red;

//			GetComponent<Renderer>().material.color = Color.red; 
			delay = Time.time + 2.0f; 
		}




	}
}
