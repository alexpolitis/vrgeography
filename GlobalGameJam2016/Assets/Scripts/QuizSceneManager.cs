﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class QuizSceneManager : MonoBehaviour {

	public QuizManager quizManager;
	public string landmarkName;
	[SerializeField] private TextMeshProUGUI informationalText;
	[SerializeField] private TextMeshProUGUI landmarkNameText;
	public Text shortText;
	public Text longText;
	string[] buttonAnswers = new string[4];
	[SerializeField] private GameObject quizButtonPanel;
	public QuizButton[] quizButtons;
	[SerializeField] private GameObject nextScenePanel;
	[SerializeField] private GameObject _correctExplosionPrefab;

	public int numTries = 4;

	// Use this for initialization
	void Start () {
		
	}

	public void populateButtons(List<string> possibleAnswers) {

		buttonAnswers[0] = landmarkName;
		possibleAnswers.Remove(landmarkName);

		for (int i = 1; i < buttonAnswers.Length; i++) {
			string randomAnswer = possibleAnswers[Random.Range(0,possibleAnswers.Count)];
			buttonAnswers[i] = randomAnswer; 
			possibleAnswers.Remove(randomAnswer);
		}

		string[] shuffledAnswers = RandomizeArr(buttonAnswers);
	
		for (int i = 0; i < quizButtons.Length; i++) {
			quizButtons[i].SetAnswer(shuffledAnswers[i]);

			if (shuffledAnswers[i] == landmarkName) {
				quizButtons[i].isCorrect = true;
			} else {
				quizButtons[i].isCorrect = false;

			}
		}

	}


	string[] RandomizeArr(string[] theArr)
	{
		for (int i = theArr.Length - 1; i > 0; i--) {
			var r = Random.Range(0,i);
			string tmp = theArr[i];
			theArr[i] = theArr[r];
			theArr[r] = tmp;
		}

		return theArr;
	}

	public void setInformationalText(Text text) {
		informationalText.text = text.text;
	}

	public void showNextScenePanel() {
		nextScenePanel.SetActive(true);
		LeanTween.value(nextScenePanel, 0f, 1f, 1f).setOnUpdate( (float _a) => {
			nextScenePanel.GetComponent<CanvasGroup>().alpha = _a;	
		});
		landmarkNameText.text = landmarkName;
	}

	public void hideQuizButtonPanel() {
		StartCoroutine(hideQuizButtonPanelCoroutine());
	}

	IEnumerator hideQuizButtonPanelCoroutine() {

		LeanTween.value(nextScenePanel, 1f, 0f, 1f).setOnUpdate( (float _a) => {
			quizButtonPanel.GetComponent<CanvasGroup>().alpha = _a;
		});

		while (LeanTween.isTweening()) { yield return null; }
		quizButtonPanel.SetActive(false);

	}

	public void showExplosion() {
		GameObject explosion = Instantiate(_correctExplosionPrefab, quizButtonPanel.transform.position, Quaternion.identity) as GameObject;
		Destroy(explosion, 5f);
	}

	public void loadNextLevel() {
		quizManager.loadNextLevel();
	}

}
