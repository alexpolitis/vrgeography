﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class TouchDemo : MonoBehaviour {

	void OnEnable() {
		GetComponent<TapGesture>().Tapped += tappedHandler;
	}

	void OnDisable() {
		GetComponent<TapGesture>().Tapped -= tappedHandler;

	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void tappedHandler(object sender, System.EventArgs e) {
		print ("tap tap taparoo");
	}
}
