﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;


public class NewGameButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ReturnToStartScreen() {
		GameObject quizManager = GameObject.FindGameObjectWithTag ("QuizManager");
		Destroy(quizManager);
		SceneManager.LoadScene("StartGameScene");
	}

}
